module bitbucket.org/makononov/gort

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/gosuri/uilive v0.0.4 // indirect
	github.com/gosuri/uiprogress v0.0.1
	github.com/mattn/go-isatty v0.0.12 // indirect
)

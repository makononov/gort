package main

import (
	"fmt"
	"os"

	"bitbucket.org/makononov/gort/scene"
	"github.com/davecgh/go-spew/spew"
)

func main() {
	if len(os.Args) != 2 {
		if err := usage(); err != nil {
			os.Exit(1)
		}
		os.Exit(0)
	}

	filename := os.Args[1]
	s, err := scene.ParseFile(filename)
	if err != nil {
		panic(err)
	}
	spew.Dump(s)

	r := NewRenderer(s, 1920, 1080, "out.png")
	if err := r.Render(); err != nil {
		panic(err)
	}
}

func usage() error {
	_, err := fmt.Fprintf(os.Stderr, `Usage: %s scene_file

Renders scene_file to out.png
`, os.Args[0])

	return err
}

package main

import (
	"image"
	"image/color"
	"image/png"
	"math"
	"os"
	"sync"

	"github.com/gosuri/uiprogress"

	"bitbucket.org/makononov/gort/scene"
	"bitbucket.org/makononov/gort/sceneelement"
	"bitbucket.org/makononov/gort/space"
)

// Renderer is the rendering engine that draws a scene
type Renderer struct {
	ImageWidth  int
	ImageHeight int
	MaxDepth    int
	Filename    string
	Scene       *scene.Scene
}

// NewRenderer creates a new Renderer object with some sensible defaults
func NewRenderer(scene *scene.Scene, width int, height int, filename string) *Renderer {
	r := Renderer{}

	r.ImageWidth = width
	r.ImageHeight = height
	r.MaxDepth = 12
	r.Filename = filename
	r.Scene = scene

	return &r
}

// Render takes a scene file and renders it into an image that is saved to the filename
// defined in the Renderer
func (r *Renderer) Render() error {
	upLeft := image.Point{0, 0}
	botRight := image.Point{r.ImageWidth, r.ImageHeight}

	img := image.NewRGBA(image.Rectangle{upLeft, botRight})

	var wg sync.WaitGroup
	uiprogress.Start()
	bar := uiprogress.AddBar(r.ImageWidth * r.ImageHeight)
	bar.AppendCompleted()
	// Iterate over the image plane
	for w := 0; w < r.ImageHeight; w++ {
		for u := 0; u < r.ImageWidth; u++ {
			wg.Add(1)
			go r.CameraRay(img, u, w, &wg, bar)
		}
	}

	wg.Wait()
	uiprogress.Stop()

	f, err := os.Create(r.Filename)
	if err != nil {
		return err
	}

	return png.Encode(f, img)
}

// CameraRay returns the resulting color of firing a ray through a given image pixel
func (r *Renderer) CameraRay(img *image.RGBA, u int, w int, wg *sync.WaitGroup, bar *uiprogress.Bar) {
	defer wg.Done()
	origin := r.Scene.Camera.Location
	depth := r.MaxDepth

	direction, err := r.CameraRayDirection(u, w)
	if err != nil {
		panic(err)
	}

	color, err := r.FireRay(&space.Ray{Origin: origin, Direction: direction}, depth)
	if err != nil {
		panic(err)
	}

	img.Set(u, w, color)
	bar.Incr()
}

// CameraRayDirection returns a direction vector for a camera ray through a given pixel in the image plane
func (r *Renderer) CameraRayDirection(i int, j int) (*space.Vector, error) {
	cam := r.Scene.Camera
	n := cam.Direction.Scale(-1)
	up := space.NewVector3(0, 1, 0)
	u := n.Cross3(*up).Normalize()
	v := n.Cross3(*u)

	aspectRatio := float64(r.ImageWidth) / float64(r.ImageHeight)
	h := math.Tan(cam.FoV*(math.Pi/360)) * 2 * cam.FocalLength
	w := h * aspectRatio

	center, err := cam.Location.Sub(*(n.Scale(cam.FocalLength)))
	centerLeft, err := center.Sub(*u.Scale(w / 2.0))
	lowerLeft, err := centerLeft.Sub(*v.Scale(h / 2.0))
	column, err := lowerLeft.Add(*u.Scale(float64(i) * (w / float64(r.ImageWidth))))
	pixelLocation, err := column.Add(*v.Scale(float64(j) * (h / float64(r.ImageHeight))))
	direction, err := pixelLocation.Sub(*cam.Location)
	if err != nil {
		return nil, err
	}

	return direction.Normalize(), nil
}

// FireRay returns the color result of a ray
func (r *Renderer) FireRay(ray *space.Ray, maxdepth int) (color.Color, error) {
	if maxdepth < 0 {
		return color.Black, nil
	}

	shape, dist, err := r.firstIntersect(ray)
	if err != nil {
		return nil, err
	}

	if shape == nil {
		return r.Scene.BackgroundColor, nil
	}

	intersectPoint, err := r.Scene.Camera.Location.Add(*ray.Direction.Scale(dist))
	if err != nil {
		return nil, err
	}

	c, _ := shape.ColorAt(intersectPoint)
	n, _ := shape.NormalAt(intersectPoint)
	viewDir, _ := r.Scene.Camera.Location.Sub(*intersectPoint)
	viewDir = viewDir.Normalize()
	specH, _ := shape.Specular()

	ambientPart := r.ambientComponent(shape, intersectPoint, c.(color.RGBA))

	diffuse := color.RGBA{}
	specular := color.RGBA{}
	for _, l := range r.Scene.Lights.Lights {
		d, s := r.lightComponents(l, intersectPoint, c.(color.RGBA), n, viewDir, specH)
		diffuse = SumRGBA(diffuse, d)
		specular = SumRGBA(specular, s)
	}

	return SumRGBA(ambientPart, SumRGBA(diffuse, specular)), nil

}

func (r *Renderer) firstIntersect(ray *space.Ray) (sceneelement.Shape, float64, error) {
	var (
		firstShape sceneelement.Shape
		distance   float64
	)

	distance = math.Inf(1)

	for _, s := range r.Scene.Shapes {
		dist, err := s.Intersects(ray)
		if err != nil {
			return nil, 0, err
		}
		if dist > 0 && dist < distance {
			firstShape = s
			distance = dist
		}
	}

	return firstShape, distance, nil
}

func (r *Renderer) lightComponents(l sceneelement.PointLight, point *space.Vector, c color.RGBA, n *space.Vector, viewDir *space.Vector, hardness float64) (color.RGBA, color.RGBA) {
	lightDir, _ := l.Location.Sub(*point)
	distance := lightDir.Length()
	lightDir = lightDir.Normalize()
	distance = distance * distance

	// to check for shadow, we originate the ray at a point a tiny bit above the surface
	origin, _ := point.Add(*n.Scale(0.0000001))
	ray := space.Ray{origin, lightDir}
	if shadow, dist, _ := r.firstIntersect(&ray); shadow != nil && dist < distance {
		// light is shadowed, don't calculate
		return color.RGBA{}, color.RGBA{}
	}

	// calc diffuse component
	ndotl, _ := n.Dot(*lightDir)
	intensity := Saturate(ndotl, 0.0, 1.0)
	diffuseColor := UniformScaleRGBA(l.Color.(color.RGBA), intensity)
	diffuseComponent := ScaleRGBA(c, diffuseColor)

	// blinn phong
	h, _ := lightDir.Add(*viewDir)
	h = h.Normalize()
	hdotn, _ := h.Dot(*n)
	intensity = math.Pow(Saturate(hdotn, 0.0, 1.0), hardness)
	specularComponent := UniformScaleRGBA(l.Color.(color.RGBA), intensity)

	return diffuseComponent, specularComponent
}

func (r *Renderer) ambientComponent(s sceneelement.Shape, point *space.Vector, c color.RGBA) color.RGBA {
	ambientColor := r.Scene.Lights.Ambient.(color.RGBA)
	return ScaleRGBA(c, ambientColor)
}

package scene

import (
	"encoding/xml"
	"fmt"
	"image/color"
	"os"

	"bitbucket.org/makononov/gort/sceneelement"
	"bitbucket.org/makononov/gort/space"
)

// Scene contains the elements of a scene to be rendered
type Scene struct {
	Camera          sceneelement.Camera
	BackgroundColor color.Color

	Lights struct {
		Ambient color.Color
		Lights  []sceneelement.PointLight
	}

	Shapes []sceneelement.Shape
}

type sceneXML struct {
	BGColor      string `xml:"bgColor,attr"`
	AmbientLight string `xml:"ambientLight,attr"`
	Camera       struct {
		Location    string  `xml:"location,attr"`
		LookAt      string  `xml:"lookAt,attr"`
		FocalLength float64 `xml:"focalLength,attr"`
		FoV         float64 `xml:"fov,attr"`
	} `xml:"camera"`
	Lights struct {
		Light []struct {
			XMLName  xml.Name
			Location string `xml:"location,attr"`
			Color    string `xml:"color,attr"`
		} `xml:",any"`
	} `xml:"lights"`
	Shapes struct {
		Shape []sceneelement.ShapeXML `xml:",any"`
	} `xml:"shapes"`
}

// ParseFile parses an XML file into a scene object
func ParseFile(filename string) (*Scene, error) {
	sceneFile, err := os.Open(filename)
	if err != nil {
		return nil, err
	}

	sceneXML := sceneXML{}
	decoder := xml.NewDecoder(sceneFile)
	if err := decoder.Decode(&sceneXML); err != nil {
		panic(err)
	}
	fmt.Printf("Decoded XML: %+v\n\n", sceneXML)
	scene, err := NewScene(sceneXML)
	if err != nil {
		return nil, err
	}
	return scene, nil
}

// NewScene creates a new scene object from XML
func NewScene(xml sceneXML) (*Scene, error) {
	scene := Scene{}
	var (
		err          error
		location     *space.Vector
		lookat       *space.Vector
		bgcolor      color.Color
		ambientlight color.Color
	)

	location, err = space.NewVectorFromString(xml.Camera.Location)
	lookat, err = space.NewVectorFromString(xml.Camera.LookAt)
	bgcolor, err = sceneelement.ParseColorString(xml.BGColor)
	ambientlight, err = sceneelement.ParseColorString(xml.AmbientLight)

	if err != nil {
		return nil, err
	}

	scene.Camera.Location = location
	scene.Camera.SetLookat(*lookat)
	scene.Camera.FocalLength = xml.Camera.FocalLength
	scene.Camera.FoV = xml.Camera.FoV

	scene.BackgroundColor = bgcolor
	scene.Lights.Ambient = ambientlight

	for _, light := range xml.Lights.Light {
		switch light.XMLName.Local {
		case "pointLight":
			location, err := space.NewVectorFromString(light.Location)
			if err != nil {
				return nil, err
			}
			color, err := sceneelement.ParseColorString(light.Color)
			if err != nil {
				return nil, err
			}
			l := sceneelement.PointLight{}
			l.Location = location
			l.Color = color
			scene.Lights.Lights = append(scene.Lights.Lights, l)
		default:
			return nil, fmt.Errorf("Unknown light type: %s", light.XMLName.Local)
		}
	}

	for _, shape := range xml.Shapes.Shape {
		switch shape.XMLName.Local {
		case "sphere":
			sphere, err := sceneelement.NewSphereFromXML(shape)
			if err != nil {
				return nil, err
			}
			scene.Shapes = append(scene.Shapes, sphere)
		default:
			return nil, fmt.Errorf("Unknown shape type: %s", shape.XMLName.Local)
		}
	}

	return &scene, nil
}

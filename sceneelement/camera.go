package sceneelement

import "bitbucket.org/makononov/gort/space"

// A Camera represents the source of rays in a scene
type Camera struct {
	SceneElement
	Direction   *space.Vector
	FocalLength float64
	FoV         float64
}

// SetLookat takes a look at vector and uses it to set the direction of the camera
func (c *Camera) SetLookat(v space.Vector) error {
	dir, err := v.Sub(*c.Location)
	if err != nil {
		return err
	}

	c.Direction = dir.Normalize()
	return nil
}

package sceneelement

import "image/color"

// PointLight is a light that radiates light evenly in all directions
type PointLight struct {
	SceneElement
	Color color.Color
}

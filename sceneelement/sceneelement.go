package sceneelement

import (
	"image/color"
	"strconv"
	"strings"

	"bitbucket.org/makononov/gort/space"
)

// SceneElement represents a generic raytraced object at a specific location
type SceneElement struct {
	Location *space.Vector
}

// ParseColorString takes an encoded color string and returns a valid color or an error
func ParseColorString(colorString string) (color.Color, error) {
	valStrings := strings.Split(colorString, ",")
	var (
		err error
		r   int64
		g   int64
		b   int64
	)

	r, err = strconv.ParseInt(strings.TrimSpace(valStrings[0]), 10, 0)
	g, err = strconv.ParseInt(strings.TrimSpace(valStrings[1]), 10, 0)
	b, err = strconv.ParseInt(strings.TrimSpace(valStrings[2]), 10, 0)

	return color.RGBA{uint8(r), uint8(g), uint8(b), 0xff}, err
}

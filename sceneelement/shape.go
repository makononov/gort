package sceneelement

import (
	"encoding/xml"
	"image/color"

	"bitbucket.org/makononov/gort/space"
)

// DoesNotIntersect is a constant value to show that a shape does not intersect a given ray
const DoesNotIntersect float64 = -1

// Shape represents a shape to be drawn in the scene
type Shape interface {
	// Intersects returns a distance past the origin where a ray intersects with a shape
	Intersects(*space.Ray) (float64, error)

	// ColorAt returns the color at a given point on the shape
	ColorAt(*space.Vector) (color.Color, error)

	// NormalAt returns the normal vector for a given point on the shape
	NormalAt(*space.Vector) (*space.Vector, error)

	// Specular returns a tuple of specular hardness and intensity
	Specular() (float64, float64)
}

// ShapeXML holds the XML structure of a generic shape definition
type ShapeXML struct {
	XMLName  xml.Name
	InnerXML string     `xml:",innerxml"`
	Attrs    []xml.Attr `xml:",attr,any"`
	Material struct {
		Diffuse struct {
			Color string `xml:"color,attr,omitempty"`
		} `xml:"diffuse"`
		Specular struct {
			Intensity string `xml:"intensity,attr,omitempty"`
			Hardness  string `xml:"hardness,attr,omitempty"`
		} `xml:"specular"`
	} `xml:"material"`
}

package sceneelement

import (
	"fmt"
	"image/color"
	"math"
	"strconv"

	"bitbucket.org/makononov/gort/space"
)

// A Sphere is a shape that can be rendered as part of the scene
type Sphere struct {
	SceneElement
	Radius            float64
	Color             color.Color
	SpecularIntensity float64
	SpecularHardness  float64
}

// Intersects implements shape.Intersects
func (s *Sphere) Intersects(ray *space.Ray) (float64, error) {
	var (
		l         *space.Vector
		o         *space.Vector
		c         *space.Vector
		r         float64
		oSubC     *space.Vector
		oSubCDotL float64
		err       error
	)
	l = ray.Direction
	o = ray.Origin
	c = s.Location
	r = s.Radius
	if oSubC, err = o.Sub(*c); err != nil {
		return DoesNotIntersect, err
	}
	if oSubCDotL, err = oSubC.Dot(*l); err != nil {
		return DoesNotIntersect, err
	}
	det := math.Pow(oSubCDotL, 2) - math.Pow(oSubC.Length(), 2) + math.Pow(r, 2)
	if det < 0 {
		return DoesNotIntersect, nil
	}

	if det == 0 {
		return -1 * oSubCDotL, nil
	}

	first := (-1 * oSubCDotL) + math.Sqrt(det)
	second := (-1 * oSubCDotL) - math.Sqrt(det)

	if first >= 0 && second >= 0 {
		return math.Min(first, second), nil
	}

	if first >= 0 {
		return first, nil
	}

	if second >= 0 {
		return second, nil
	}

	return DoesNotIntersect, nil
}

// ColorAt implements Shape
func (s *Sphere) ColorAt(point *space.Vector) (color.Color, error) {
	return s.Color, nil
}

// NormalAt implements Shape
func (s *Sphere) NormalAt(point *space.Vector) (*space.Vector, error) {
	vec, err := point.Sub(*s.Location)
	return vec.Normalize(), err
}

// NewSphere returns a new Sphere shape with the given parameters
func NewSphere(center *space.Vector, radius float64) *Sphere {
	s := Sphere{}
	s.Location = center
	s.Radius = radius
	return &s
}

// NewSphereFromXML generates a new sphere object given a generic ShapeXML entity
func NewSphereFromXML(xml ShapeXML) (*Sphere, error) {
	var (
		center string
		radius string
	)
	for _, a := range xml.Attrs {
		if a.Name.Local == "location" {
			center = a.Value
		} else if a.Name.Local == "radius" {
			radius = a.Value
			break
		} else {
			return nil, fmt.Errorf("Unexpected attribute value in Sphere: %s", a.Name.Local)
		}
	}

	centreVec, err := space.NewVectorFromString(center)
	radiusVal, err := strconv.ParseFloat(radius, 64)
	if err != nil {
		return nil, err
	}

	s := NewSphere(centreVec, radiusVal)
	s.Color, err = ParseColorString(xml.Material.Diffuse.Color)
	if err != nil {
		return nil, err
	}

	s.SpecularIntensity, err = strconv.ParseFloat(xml.Material.Specular.Intensity, 64)
	if err != nil {
		return nil, err
	}

	s.SpecularHardness, err = strconv.ParseFloat(xml.Material.Specular.Hardness, 64)
	if err != nil {
		return nil, err
	}

	return s, nil
}

// Specular implements shape.Specular()
func (s *Sphere) Specular() (float64, float64) {
	return s.SpecularHardness, s.SpecularIntensity
}

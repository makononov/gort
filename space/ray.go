package space

// A Ray is a vector that has an origin and direction
type Ray struct {
	Origin    *Vector
	Direction *Vector
}

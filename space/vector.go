package space

import (
	"errors"
	"math"
	"strconv"
	"strings"
)

// Vector stores the values of a vector in n-space
type Vector struct {
	Values []float64
}

// NewVector returns a new vector of the passed in dimension
func NewVector(length int) *Vector {
	v := Vector{make([]float64, length)}
	return &v
}

// NewVector3 returns a new 3-dimensional vector
func NewVector3(x float64, y float64, z float64) *Vector {
	v := Vector{[]float64{x, y, z}}
	return &v
}

// NewVectorFromString Takes a comma-separated list of values and creates a new vector from them
func NewVectorFromString(vals string) (*Vector, error) {
	values := strings.Split(vals, ",")
	vec := NewVector(len(values))
	for i, v := range values {
		val, err := strconv.ParseFloat(strings.TrimSpace(v), 64)
		if err != nil {
			return nil, err
		}
		vec.Values[i] = val
	}
	return vec, nil
}

// N returns the number of dimensions in the space of the vector
func (v Vector) N() int {
	return len(v.Values)
}

// X returns the first dimensional component of the vector
func (v Vector) X() float64 {
	return v.Values[0]
}

// Y returns the second dimensional component of the vector
func (v Vector) Y() float64 {
	return v.Values[1]
}

// Z returns the third dimensional component of the vector
func (v Vector) Z() float64 {
	return v.Values[2]
}

// Length returns the length of the vector
func (v Vector) Length() float64 {
	var sum float64
	for _, v := range v.Values {
		sum += (v * v)
	}
	return math.Sqrt(sum)
}

// Scale returns a new vector with the same direction, scaled by the passed scalar value
func (v Vector) Scale(scalar float64) *Vector {
	w := Vector{make([]float64, len(v.Values))}
	for i, j := range v.Values {
		w.Values[i] = j * scalar
	}
	return &w
}

// Negate returns a vector in the opposite direction
func (v Vector) Negate() *Vector {
	return v.Scale(-1)
}

// Normalize returns a new unit vector with the same direction as the given vector
func (v Vector) Normalize() *Vector {
	if v.Length() == 0 {
		return v.Scale(0)
	}
	return v.Scale(1 / v.Length())
}

// Dot returns the dot product of this vector and the passed vector
func (v Vector) Dot(w Vector) (float64, error) {
	if len(v.Values) != len(w.Values) {
		return 0, errors.New("Cannot find the dot product of vectors with different lengths")
	}

	var sum float64
	for i := range v.Values {
		sum += v.Values[i] * w.Values[i]
	}

	return sum, nil
}

// Cross3 returns the cross product of 3-dimensional vectors v, w
func (v Vector) Cross3(w Vector) *Vector {
	x := (v.Y() * w.Z()) - (v.Z() * w.Y())
	y := (v.Z() * w.X()) - (v.X() * w.Z())
	z := (v.X() * w.Y()) - (v.Y() * w.X())
	vec := NewVector3(x, y, z)
	return vec
}

// Add adds a vector to another vector
func (v Vector) Add(w Vector) (*Vector, error) {
	length := v.N()
	if w.N() != length {
		return nil, errors.New("Attempt to add vectors of different dimensions")
	}

	r := NewVector(length)
	for i := 0; i < length; i++ {
		r.Values[i] = v.Values[i] + w.Values[i]
	}
	return r, nil
}

// Sub subtract one vector from another
func (v Vector) Sub(w Vector) (*Vector, error) {
	if v.N() != w.N() {
		return nil, errors.New("Attempt to subtract vectors of different dimensions")
	}

	return v.Add(*w.Scale(-1))
}

// Equals returns true if the two vectors have the same dimensions, length, and direction; false otherwise
func (v Vector) Equals(w Vector) bool {
	if v.N() != w.N() {
		return false
	}
	for i := 0; i < v.N(); i++ {
		if v.Values[i] != w.Values[i] {
			return false
		}
	}

	return true
}

// Reflect gives a reflection vector using a incident vector and surface normal
func (v Vector) Reflect(n Vector) *Vector {
	// 𝑟=𝑑−2(𝑑⋅𝑛)𝑛
	vdotn, _ := v.Dot(n)
	r, _ := v.Sub(*(n.Scale(2 * vdotn)))
	return r
}

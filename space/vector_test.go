package space

import (
	"math"
	"testing"
)

func TestVectorN(t *testing.T) {
	v := NewVector3(1, 1, 1)
	if v.N() != 3 {
		t.Errorf("Vector3 has incorrect dimesions: %d", v.N())
	}
}
func TestVectorLength(t *testing.T) {
	v := NewVector3(6, -6, 3)
	if v.Length() != 9 {
		t.Errorf("Unexpected vector length: %f", v.Length())
	}
}

func TestVectorScale(t *testing.T) {
	expected := [3]float64{3, 1.5, -6}
	var actual [3]float64
	v := NewVector3(6, 3, -12).Scale(0.5)
	copy(actual[:], v.Values)
	if actual != expected {
		t.Errorf("Unexpected values of scaled vector: %v", actual)
	}
}

func TestVectorNormalize(t *testing.T) {
	v := NewVector3(8, -3, 5)
	n := v.Normalize()
	if math.Abs(n.Length()-1.0) > 0.0000001 {
		t.Errorf("Normal vector has a length of %f (expected 1.0)", n.Length())
	}
}

func TestVectorDotProduct(t *testing.T) {
	v := NewVector3(1, 3, -5)
	w := NewVector3(4, -2, -1)
	d, err := v.Dot(*w)
	if err != nil {
		t.Errorf("Error calculating dot product: %s", err)
	}
	e, er := w.Dot(*v)
	if er != nil {
		t.Errorf("Error calculating dot product: %s", er)
	}
	if d != e {
		t.Errorf("Dot product not commutative (%f != %f)", d, e)
	}
	if d != 3 {
		t.Errorf("Incorrect value obtained for dot product: %f", d)
	}
}

func TestVectorAdd(t *testing.T) {
	v := NewVector3(50, 12, 13)
	w := NewVector3(18, 30, -6)
	s, err := v.Add(*w)
	if err != nil {
		t.Errorf("Error adding vectors: %s", err)
	}

	d, errd := v.Sub(*w)
	if errd != nil {
		t.Errorf("Error subtracting vectors: %s", errd)
	}

	if !s.Equals(*NewVector3(68, 42, 7)) {
		t.Error("Added vector result not expected")
	}

	if !d.Equals(*NewVector3(32, -18, 19)) {
		t.Error("Subtracted vector result not expected")
	}
}

func TestVectorEquals(t *testing.T) {
	v := NewVector3(3, 5, 9)
	w := NewVector3(3, 5, 9)
	u := NewVector3(1, 5, 6)
	r := NewVector3(3, 4, 9)

	if !v.Equals(*w) {
		t.Error("Expected equal vectors, but found unequal")
	}

	if v.Equals(*u) || v.Equals(*r) || u.Equals(*r) {
		t.Errorf("Expected unequal vectors but found equal")
	}
}

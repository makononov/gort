package main

import (
	"image/color"
)

// Filter function for float64 slices
func Filter(vs []float64, f func(float64) bool) []float64 {
	vsf := make([]float64, 0)
	for _, v := range vs {
		if f(v) {
			vsf = append(vsf, v)
		}
	}
	return vsf
}

// ScaleRGBA returns the first color scaled by the values of the second color
func ScaleRGBA(a color.RGBA, b color.RGBA) color.RGBA {
	red := uint8(float64(a.R) * float64(b.R) / 255.0)
	green := uint8(float64(a.G) * float64(b.G) / 255.0)
	blue := uint8(float64(a.B) * float64(b.B) / 255.0)
	alpha := uint8(float64(a.A) * float64(b.A) / 255.0)
	return color.RGBA{red, green, blue, alpha}
}

// UniformScaleRGBA scales the passed color by a uniform factor
func UniformScaleRGBA(a color.RGBA, factor float64) color.RGBA {
	f := Saturate(factor, 0.0, 1.0)
	red := uint8(float64(a.R) * f)
	green := uint8(float64(a.G) * f)
	blue := uint8(float64(a.B) * f)
	alpha := uint8(float64(a.A) * f)
	return color.RGBA{red, green, blue, alpha}
}

// SumRGBA sums all of the passed in colors and returns the result. Individual values cannot exceed 255
func SumRGBA(colors ...color.RGBA) color.RGBA {
	result := color.RGBA{}
	for _, c := range colors {
		var (
			red   int16
			green int16
			blue  int16
			alpha int16
		)
		red = MinInt16(int16(result.R)+int16(c.R), 255)
		result.R = uint8(red)

		green = MinInt16(int16(result.G)+int16(c.G), 255)
		result.G = uint8(green)

		blue = MinInt16(int16(result.B)+int16(c.B), 255)
		result.B = uint8(blue)

		alpha = MinInt16(int16(result.A)+int16(c.A), 255)
		result.A = uint8(alpha)
	}

	return result
}

// MinInt16 returns the smaller of two int16 values
func MinInt16(a int16, b int16) int16 {
	if a < b {
		return a
	}
	return b
}

// Saturate clamps a value between a minimum and maximum value
func Saturate(val float64, min float64, max float64) float64 {
	if val < min {
		return min
	}
	if val > max {
		return max
	}
	return val
}
